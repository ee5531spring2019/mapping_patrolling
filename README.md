# mapping_patrolling
Parameters from move_base have been tuned:

obstacle_range: 3.0

inflation_radius: 1.0

controller_patience: 10.0

planner_patience: 2.5

acc_lim_x: 10.0

max_vel_x: 1.0

occdist_scale:  0.2

for global cost map:

width: 80.0

height: 80.0

# Insutrction

Run the following code in the terminal:

1. cd ~/catkin_ws
2. catkin_make
3. source ./devel/setup.bash
4. roslaunch mapping_patrolling jack_waypoint_patroling.launch
