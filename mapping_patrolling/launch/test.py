#!/usr/bin/env python

import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

#waypoints=[8.637,-8.08257,0.0635,
#       -8.03134,-4.719,0.0635,
#       -1.636,6.340,0.0635,
#       0.637015,-4.121,0.0635,
#       4.638,4.817,0.0635,
#       -3.10013,-1.028,0.0635]  

def movebase_client(xGoal,yGoal):
    client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    client.wait_for_server()
    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position.x = xGoal
    goal.target_pose.pose.position.y = yGoal
    goal.target_pose.pose.position.z = 0
    goal.target_pose.pose.orientation.w = 1.0
    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server is not available!")
        rospy.signal_shutdown("Action server is not available!")
    else:
        return client.get_result()
        
if __name__ == '__main__':
    try:
       rospy.init_node('movebase_client')
#       result = movebase_client(8.637, 8.08257)
#       if result:
#         rospy.loginfo("Goal has been reached!")
    except rospy.ROSInterrupstException:
       rospy.loginfo("Navigation is finished.")

