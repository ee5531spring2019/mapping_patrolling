#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
#keyboard mapping of w, x, a, d, s
# uses a Python dictionary to store the mapping between keystrokes and the target velocities
key_mapping = { 'w': [ 0, 1], 'x': [0, -1], 
                'a': [-1, 0], 'd': [1,  0], 
                's': [ 0, 0] }


def getkeys(msg, send_pub):
  #If a key is found, the target velocities are extracted from the dictionary

  if len(msg.data) == 0 or not key_mapping.has_key(msg.data[0]):
    return 
  vels = key_mapping[msg.data[0]]
  #print vels
  
  vel = Twist()
  vel.angular.z = vels[0]
  vel.linear.x  = vels[1]
  send_pub.publish(vel)

if __name__ == '__main__':
  rospy.loginfo("To stop Simulation CTRL + C")
  rospy.init_node('jackaa')
   #Creat publisher to talk to Jack and make it move

  send_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
  # recieve the command
  rospy.Subscriber('jack', String, getkeys, send_pub)
  rospy.spin()

