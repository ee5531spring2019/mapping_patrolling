#!/usr/bin/env python

import sys, select, tty, termios
import rospy
from std_msgs.msg import String

if __name__ == '__main__':
  key_pub = rospy.Publisher('jack', String, queue_size=1)
  rospy.init_node('filtring_speed')
  rate = rospy.Rate(100)
  # to solve the problem of enter in linux
  old_attr = termios.tcgetattr(sys.stdin)
  tty.setcbreak(sys.stdin.fileno())
  
  print "Publishing keystrokes. Press Ctrl-C to exit..."
  while not rospy.is_shutdown():
    # continually poll the stdin stream to see if there is any char ready
    if select.select([sys.stdin], [], [], 0)[0] == [sys.stdin]:
      key_pub.publish(sys.stdin.read(1))
    rate.sleep()

  # put the consol back as it was
  termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_attr)

