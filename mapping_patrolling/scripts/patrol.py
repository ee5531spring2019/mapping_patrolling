#!/usr/bin/env python

import rospy
import actionlib
import time
import math
import numpy as np
from operator import itemgetter
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Twist, Vector3, PoseWithCovarianceStamped, Quaternion, Pose

waypoints=[# 0.2,0.5,0,
       -3.10013,-1.028,0.0635,
       4.638,4.817,0.0635,
#       5.5800,-1.3136,0.0635,
       0.637015,-4.121,0.0635,
#       5.6806,8.4034,0.0635,
       -1.636,6.340,0.0635,
       -8.03134,-4.719,0.0635,
       8.637,-8.08257,0.0635]
       

desired = [0, 0, 0]

def movebase_client(xGoal,yGoal):
    client = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    client.wait_for_server()
    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position=Point(xGoal,yGoal,0)
    goal.target_pose.pose.orientation.w = 1.0
    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server is not available!")
        rospy.signal_shutdown("Action server is not available!")
    else:
        return client.get_result()

def callback(msg):
  #initial position offset
  x_spawn = 0.0
  y_spawn = 0.0
  
  #count for waypoints input
  m = 3
  
  #initialize lists
  dist = list()
  points = list()

  #read waypoints and calcualte distance
  for j in range(0, len(waypoints), m):
    dist.append(math.sqrt((msg.pose.pose.position.x - (waypoints[j]+x_spawn))**2 + (msg.pose.pose.position.y - (waypoints[j+1]+y_spawn))**2))
    points.append(waypoints[j:j+m])
  
  #pick local minimum distance
  k = np.argmin(dist)
  #assign the closest waypoint
  desired[0] = waypoints[k*3]
  desired[1] = waypoints[k*3+1]
  desired[2] = waypoints[k*3+2]
  #print the closest waypoint
  print desired
  #call movebase function
  dresult = movebase_client((desired[0]+x_spawn), (desired[1]+y_spawn))

  if dresult:
    rospy.loginfo("Closest point reached!")
    
    #remove the closest waypoint from list
    points.remove(desired)

  for point in points:
      #print ongoing waypoint
      print point
      #call movebase function
      result = movebase_client((point[0]+x_spawn), (point[1]+y_spawn))
  if result:
      rospy.loginfo("Goal has been reached!")

if __name__ == '__main__':
    try:
       rospy.init_node('movebase_client')
       
       #subscribe from odometry node and call function
       odom = rospy.Subscriber("/odometry/filtered", Odometry, callback)

       time.sleep(20)
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation is finished.")
    
    rospy.spin()
        
